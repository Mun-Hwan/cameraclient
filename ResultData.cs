﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraClient
{
    public class PictureInfo
    {
        public string FileName { get; set; }
        public string FileId { get; set; }
        public string FileLocation { get; set; }
    }

    public class DiagnosticInfo
    {
        public string Name { get; set; }
        public string PestName { get; set; }
        public string Accuracy { get; set; }
        public string SymptomLeaf { get; set; }
        public string SymptomStem { get; set; }
        public string Symptomfruit { get; set; }
        public string SymptomEtc { get; set; }
        public string Cultural { get; set; }
        public string Soil { get; set; }
    }

    public class ResolveInfo
    {
        public string PestName { get; set; }
        public string ProductName { get; set; }
        public string HumanToxic { get; set; }
        public string MakeCoporation { get; set; }
        public string UseTime { get; set; }
        public string WaterUse { get; set; }
        public string SafetyUse { get; set; }
        public string SafetyCount { get; set; }
    }

    public class ResSmartFarm
    {
        public PictureInfo PictureInfo { get; set; }
        public DiagnosticInfo DiagnosticInfo { get; set; }
        public List<ResolveInfo> ResolveInfos { get; set; }
    }
}
