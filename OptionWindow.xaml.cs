﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CameraClient
{
    /// <summary>
    /// Interaction logic for OptionWindow.xaml
    /// </summary>
    public partial class OptionWindow : Window
    {
        public OptionWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 적용
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (TB_HOST.Text == null || TB_HOST.Text.Length == 0)
            {
                MessageBox.Show("호스트를 입력해주세요.");
                return;
            }

            if (TB_PORT.Text == null || TB_PORT.Text.Length == 0)
            {
                MessageBox.Show("포트를 입력해주세요.");
                return;
            }

            try
            {
                int port = Convert.ToInt32(TB_PORT.Text);
                if (port <= 0 || port >= UInt16.MaxValue)
                {
                    MessageBox.Show("잘못된 포트값입니다.");
                    return;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("잘못된 포트값입니다.");
                return;
            }
            
            if (SharedPreference.Instance.Config.SelectedHouse == null)
            {
                MessageBox.Show("온실코드를 선택해주세요.");
                return;
            }
            
            DialogResult = true;
        }

        /// <summary>
        /// 취소
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// 드래그
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        /// <summary>
        /// 닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// 코드관리 윈도우
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            new HouseCodeWindow().ShowDialog();
        }

        //if (MessageBox.Show($"온실코드 {SharedPreference.Instance.Config.SelectedHouse.HouseCode} 를 정말 삭제하시겠습니까?", "알림", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
        //    {
        //        return;
        //    }
    }
}
