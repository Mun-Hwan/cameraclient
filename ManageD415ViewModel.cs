﻿using D415LIb;
using Intel.RealSense;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CameraClient
{
    public class ManageD415ViewModel : BindableBase
    {
        #region property

        private ManageD415 uc;
        private Pipeline pipeline;
        private Colorizer colorizer;
        private CancellationTokenSource tokenSource;

        /// <summary>
        /// 선택된 로봇 위치
        /// </summary>
        private string m_SelectedRobotPosition;
        public string SelectedRobotPosition
        {
            get
            {
                return m_SelectedRobotPosition;
            }
            set
            {
                m_SelectedRobotPosition = value;
                RaisePropertyChanged("SelectedRobotPosition");
            }
        }

        /// <summary>
        /// 선택된 카메라 방향
        /// </summary>
        private KeyValuePair<string, string> m_SelectedCameraDirection;
        public KeyValuePair<string, string> SelectedCameraDirection
        {
            get
            {
                return m_SelectedCameraDirection;
            }
            set
            {
                m_SelectedCameraDirection = value;
                RaisePropertyChanged("SelectedCameraDirection");
            }
        }

        /// <summary>
        /// 카메라 연결 여부
        /// </summary>
        private bool m_IsConnected = false;
        public bool IsConnected
        {
            get
            {
                return m_IsConnected;
            }
            set
            {
                m_IsConnected = value;
                RaisePropertyChanged("IsConnected");
            }
        }

        #endregion

        #region command

        /// <summary>
        /// 로딩 커맨드
        /// </summary>
        public DelegateCommand<ManageD415> LoadedCommand
        {
            get
            {
                return new DelegateCommand<ManageD415>(delegate (ManageD415 uc)
                {
                    this.uc = uc;

                    SelectedRobotPosition = SharedPreference.Instance.RobotPositions.ElementAt(0);
                    SelectedCameraDirection = SharedPreference.Instance.CameraDirection.ElementAt(0);
                });
            }
        }

        /// <summary>
        /// 언로딩 커맨드
        /// </summary>
        public DelegateCommand UnloadedCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    CloseCamera();
                });
            }
        }

        /// <summary>
        /// 카메라 연결
        /// </summary>
        public DelegateCommand ConnectCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    if (SharedPreference.Instance.Config.SelectedHouse == null)
                    {
                        MessageBox.Show("온실코드를 선택해주세요.");
                        return;
                    }

                    try
                    {
                        pipeline = new Pipeline();
                        colorizer = new Colorizer();
                        tokenSource = new CancellationTokenSource();

                        var cfg = new Config();
                        //cfg.EnableStream(Stream.Depth, 640, 480);
                        cfg.EnableStream(Stream.Depth, 1280, 720);
                        cfg.EnableStream(Stream.Color, Format.Rgb8);

                        pipeline.Start(cfg);

                        var token = tokenSource.Token;

                        var t = Task.Factory.StartNew(() =>
                        {
                            while (!token.IsCancellationRequested)
                            {
                                var frames = pipeline.WaitForFrames();

                                var colorized_depth = colorizer.Colorize(frames.DepthFrame);
                                UploadImage(uc.imgDepth, colorized_depth);

                                UploadImage(uc.imgColor, frames.ColorFrame);
                            }
                        }, token);

                        IsConnected = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                });
            }
        }

        /// <summary>
        /// 카메라 연결 해제
        /// </summary>
        public DelegateCommand CameraCloseCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    CloseCamera();
                });
            }
        }

        /// <summary>
        /// 사진 저장 & 전송
        /// </summary>
        public DelegateCommand SaveCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    if (SharedPreference.Instance.Config.SelectedHouse == null)
                    {
                        MessageBox.Show("온실코드를 선택해주세요.");
                        return;
                    }

                    Save();

                    //string[] paths = PhotoHelper.SaveImage(DateTime.Now, SharedPreference.Instance.Config.Farm, SharedPreference.Instance.Config.House, SelectedRobotPosition, SelectedCameraDirection.Key, uc.imgColor.Source as System.Windows.Media.Imaging.BitmapSource, uc.imgDepth.Source as System.Windows.Media.Imaging.BitmapSource);

                    //switch (SharedPreference.Instance.Config.SendType)
                    //{
                    //    case 1:
                    //        SharedPreference.Instance.SendImage(paths[1]);

                    //        SharedPreference.Instance.SendImage(paths[0]);
                    //        break;
                    //    default: break;
                    //}
                });
            }
        }

        #endregion

        #region method

        /// <summary>
        /// D415 이미지 업데이트
        /// </summary>
        /// <param name="img"></param>
        /// <param name="frame"></param>
        private void UploadImage(Image img, VideoFrame frame)
        {
            if (System.Windows.Application.Current == null) return;

            System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (frame.Width == 0) return;

                var bytes = new byte[frame.Stride * frame.Height];
                frame.CopyTo(bytes);

                var bs = System.Windows.Media.Imaging.BitmapSource.Create(frame.Width, frame.Height,
                                  300, 300,
                                  System.Windows.Media.PixelFormats.Rgb24,
                                  null,
                                  bytes,
                                  frame.Stride);

                var imgSrc = bs as System.Windows.Media.ImageSource;

                img.Source = imgSrc;
            }));
        }

        private void CloseCamera()
        {
            IsConnected = false;
            if (tokenSource != null)
            {
                tokenSource.Cancel();
            }
        }

        async private void Save()
        {
            string[] paths = PhotoHelper.SaveImage(DateTime.Now, SharedPreference.Instance.Config.SelectedHouse.HouseCode, SharedPreference.Instance.Config.SelectedHouse.HouseNoCode, SelectedRobotPosition, SelectedCameraDirection.Key, uc.imgColor.Source as System.Windows.Media.Imaging.BitmapSource, uc.imgDepth.Source as System.Windows.Media.Imaging.BitmapSource);

            await Task.Factory.StartNew(() =>
            {
                switch (SharedPreference.Instance.Config.SendType)
                {
                    case 1:
                        //SharedPreference.Instance.SendImage(paths[1]);
                        //System.Threading.Thread.Sleep(1000);
                        //SharedPreference.Instance.SendImage(paths[0]);

                        System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                        {
                            Mouse.OverrideCursor = Cursors.Wait;
                        });
                        
                        SharedPreference.Instance.ResSmartFarmsInit();

                        Task<string> returnString = SharedPreference.Instance.HttpSendImageAsync(paths[0]);
                        returnString.Wait();
                        //MessageBox.Show(returnString.Result);

                        var farm_info = JsonConvert.DeserializeObject<ResSmartFarm>(returnString.Result);

                        if (farm_info.DiagnosticInfo != null)
                        {
                            farm_info.PictureInfo.FileLocation = paths[0];
                        }
                        else
                        {
                            farm_info.PictureInfo = new PictureInfo()
                            {
                                FileLocation = paths[0]
                            };
                        }

                        SharedPreference.Instance.ResSmartFarm = farm_info;
                        SharedPreference.Instance.ResSmartFarmsLength = 1;

                        System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                        {
                            Mouse.OverrideCursor = null;
                            var window = new ResultWindow();
                            var resultDlg = window.ShowDialog();
                        });
                        
                        break;
                    default:
                        break;
                }
            });
        }

        #endregion
    }
}
