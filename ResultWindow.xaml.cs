﻿using System.Windows;
using System.Windows.Input;

namespace CameraClient
{
    /// <summary>
    /// ResultWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ResultWindow : Window
    {
        public ResultWindow()
        {
            InitializeComponent();
            ImageShow();
        }

        /// <summary>
        /// 닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// 드래그
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        /// <summary>
        /// 닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// 다음
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            var maxLength = SharedPreference.Instance.ResSmartFarmsLength - 1;
            var curIndex = SharedPreference.Instance.ResSmartFarmIdx;

            if (curIndex < maxLength)
            {
                SharedPreference.Instance.ResSmartFarmIdx++;
                ImageShow();
            }
        }

        /// <summary>
        /// 이전
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseLeftButtonUp_2(object sender, MouseButtonEventArgs e)
        {
            var curIndex = SharedPreference.Instance.ResSmartFarmIdx;

            if (curIndex > 0)
            {
                SharedPreference.Instance.ResSmartFarmIdx--;
                ImageShow();
            }
        }

        /// <summary>
        /// 전송된 이미지 보여주기
        /// </summary>
        private void ImageShow()
        {
            ResultVIEW1.ImageLocation = SharedPreference.Instance.ResSmartFarm.PictureInfo.FileLocation;
        }

        /// <summary>
        /// 최소화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            if (window.WindowState != WindowState.Minimized)
            {
                window.WindowState = WindowState.Minimized;
            }
        }

        /// <summary>
        /// 최대화/복원
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            window.WindowState = window.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }
    }
}
