﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CameraClient
{
    /// <summary>
    /// Interaction logic for HouseCodeWindow.xaml
    /// </summary>
    public partial class HouseCodeWindow : Window
    {
        public HouseCodeWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 추가
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SharedPreference.Instance.Config.Houses.Add(new HouseData());
        }

        /// <summary>
        /// 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SharedPreference.Instance.Config.Houses.Remove(DG1.SelectedItem as HouseData);
        }

        /// <summary>
        /// 닫기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SharedPreference.Instance.Config = XmlHelper.Load();
            this.Close();
        }

        /// <summary>
        /// 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            XmlHelper.Save(SharedPreference.Instance.Config);
            this.Close();
        }
    }
}
