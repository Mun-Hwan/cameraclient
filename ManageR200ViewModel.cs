﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CameraClient
{
    public class ManageR200ViewModel : BindableBase
    {
        #region property

        private R200Wrapper.CameraManager CameraManager { get; set; }

        /// <summary>
        /// 선택된 로봇 위치
        /// </summary>
        private string m_SelectedRobotPosition;
        public string SelectedRobotPosition
        {
            get
            {
                return m_SelectedRobotPosition;
            }
            set
            {
                m_SelectedRobotPosition = value;
                RaisePropertyChanged("SelectedRobotPosition");
            }
        }

        /// <summary>
        /// 선택된 카메라 방향
        /// </summary>
        private KeyValuePair<string, string> m_SelectedCameraDirection;
        public KeyValuePair<string, string> SelectedCameraDirection
        {
            get
            {
                return m_SelectedCameraDirection;
            }
            set
            {
                m_SelectedCameraDirection = value;
                RaisePropertyChanged("SelectedCameraDirection");
            }
        }

        /// <summary>
        /// 카메라 연결 여부
        /// </summary>
        private bool m_IsConnected = false;
        public bool IsConnected
        {
            get
            {
                return m_IsConnected;
            }
            set
            {
                m_IsConnected = value;
                RaisePropertyChanged("IsConnected");
            }
        }

        /// <summary>
        /// 카메라 시작 여부
        /// </summary>
        private bool m_IsCameraStarted = false;
        public bool IsCameraStarted
        {
            get
            {
                return m_IsCameraStarted;
            }
            set
            {
                m_IsCameraStarted = value;
                RaisePropertyChanged("IsCameraStarted");
            }
        }

        /// <summary>
        /// 프리뷰
        /// </summary>
        private bool m_IsPreview = false;
        public bool IsPreview
        {
            get
            {
                return m_IsPreview;
            }
            set
            {
                m_IsPreview = value;
                RaisePropertyChanged("IsPreview");
            }
        }

        #endregion

        #region command

        /// <summary>
        /// 로딩 커맨드
        /// </summary>
        public DelegateCommand<ManageR200> LoadedCommand
        {
            get
            {
                return new DelegateCommand<ManageR200>(delegate (ManageR200 uc)
                {
                    CameraManager = new R200Wrapper.CameraManager(uc.VIEW1, uc.VIEW2, ImageSaveCompletedCallback, UpdateStatusCallback);

                    SelectedRobotPosition = SharedPreference.Instance.RobotPositions.ElementAt(0);
                    SelectedCameraDirection = SharedPreference.Instance.CameraDirection.ElementAt(0);
                });
            }
        }

        /// <summary>
        /// 언로딩 커맨드
        /// </summary>
        public DelegateCommand UnloadedCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    CloseCamera();
                });
            }
        }


        /// <summary>
        /// 연결 커맨드
        /// </summary>
        public DelegateCommand ConnectCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    if (SharedPreference.Instance.Config.SelectedHouse == null)
                    {
                        MessageBox.Show("온실코드를 선택해주세요.");
                        return;
                    }
                    
                    CameraManager.Load();
                    IsConnected = true;
                    IsPreview = false;
                });
            }
        }

        /// <summary>
        /// 카메라 연결 해제
        /// </summary>
        public DelegateCommand CameraCloseCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    CloseCamera();
                });
            }
        }

        /// <summary>
        /// 프리뷰
        /// </summary>
        public DelegateCommand PreviewCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    CameraManager.Preview();
                    IsPreview = true;

                    if (!IsCameraStarted)
                    {
                        IsCameraStarted = true;
                    }
                });
            }
        }

        /// <summary>
        /// 카메라 캡쳐
        /// </summary>
        public DelegateCommand CaptureCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    CameraManager.Preview();
                    IsPreview = false;
                });
            }
        }

        /// <summary>
        /// 사진 저장 & 전송
        /// </summary>
        public DelegateCommand SaveCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    if (SharedPreference.Instance.Config.SelectedHouse == null)
                    {
                        MessageBox.Show("온실코드를 선택해주세요.");
                        return;
                    }

                    DateTime now = DateTime.Now;
                    string nowst = string.Format(@"{0:D4}{1:D2}{2:D2}{3:D2}{4:D2}{5:D2}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);

                    string path = string.Format(@"d:\img_org\{0:D4}\{1:D2}\{2:D2}", now.Year, now.Month, now.Day);
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }

                    string rgbFilename = $"{SharedPreference.Instance.Config.SelectedHouse.HouseCode}_{SharedPreference.Instance.Config.SelectedHouse.HouseNoCode}_{SelectedRobotPosition}1{SelectedCameraDirection.Key}{nowst}";

                    var ImagePath = $@"{path}\{rgbFilename}.jpg";
                    CameraManager.SaveImage(ImagePath);
                    //CameraManager.SaveImage($@"c:\img_org\{rgbFilename}.jpg");

                    IsPreview = true;
                });
            }
        }

        #endregion

        #region method

        /// <summary>
        /// 이미지 저장 완료
        /// </summary>
        /// <param name="result"></param>
        /// <param name="path"></param>
        public void ImageSaveCompletedCallback(bool result, string path)
        {
            if (result)
            {
                switch (SharedPreference.Instance.Config.SendType)
                {
                    case 1:

                        Mouse.OverrideCursor = Cursors.Wait;

                        SharedPreference.Instance.ResSmartFarmsInit();

                        //SharedPreference.Instance.SendImage(path); 
                        Task<string> returnString = SharedPreference.Instance.HttpSendImageAsync(path);
                        returnString.Wait();
                        //MessageBox.Show(returnString.Result);

                        var farm_info = JsonConvert.DeserializeObject<ResSmartFarm>(returnString.Result);

                        if (farm_info.DiagnosticInfo != null)
                        {
                            farm_info.PictureInfo.FileLocation = path;
                        }
                        else
                        {
                            farm_info.PictureInfo = new PictureInfo()
                            {
                                FileLocation = path
                            };
                        }

                        SharedPreference.Instance.ResSmartFarm = farm_info;
                        SharedPreference.Instance.ResSmartFarmsLength = 1;

                        Mouse.OverrideCursor = null;

                        var window = new ResultWindow();
                        var resultDlg = window.ShowDialog();

                        break;
                    default:
                        break;
                }
            }
        }

        public void UpdateStatusCallback(string value)
        {

        }

        private void CloseCamera()
        {
            CameraManager.CloseAll();
            IsConnected = false;
            IsPreview = false;
            IsCameraStarted = false;
        }



        #endregion
    }
}
