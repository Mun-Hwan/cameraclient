﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CameraClient
{
    [XmlRoot("XmlData")]
    [XmlInclude(typeof(HouseData))]
    public class XmlData : BindableBase
    {
        [XmlArray("Houses")]
        [XmlArrayItem("HouseDataItem")]
        private ObservableCollection<HouseData> m_Houses;
        public ObservableCollection<HouseData> Houses
        {
            get
            {
                if (m_Houses == null)
                {
                    m_Houses = new ObservableCollection<HouseData>();
                }
                return m_Houses;
            }
            set
            {
                m_Houses = value;
                RaisePropertyChanged("Houses");
            }
        }

        /// <summary>
        /// 선택한 온실코드
        /// </summary>
        private HouseData m_SelectedHouse;
        public HouseData SelectedHouse
        {
            get
            {
                return m_SelectedHouse;
            }
            set
            {
                m_SelectedHouse = value;
                RaisePropertyChanged("SelectedHouse");
            }
        }
        
        /// <summary>
        /// 클라우드 서버 호스트
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 클라우드 서버 포트
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 전송타입
        /// 1: 실시간 전송
        /// 2: 폴더
        /// 3: 파일 선택
        /// </summary>
        private int m_SendType;
        public int SendType
        {
            get
            {
                return m_SendType;
            }
            set
            {
                m_SendType = value;
                RaisePropertyChanged("SendType");
            }
        }

        #region method

        /// <summary>
        /// 초기값을 생성한다.
        /// </summary>
        public void CreateInitValues()
        {
            Host = "222.105.187.66";
            Port = 51101;
            SendType = 3;
        }

        /// <summary>
        /// 온실코드 삭제
        /// </summary>
        /// <param name="data"></param>
        public void Delete(HouseData data)
        {
            Houses.Remove(data);
            XmlHelper.Save(this);
        }

        /// <summary>
        /// 온실코드 변경
        /// </summary>
        /// <param name="data"></param>
        public void Update(HouseData data)
        {
            XmlHelper.Save(this);
        }

        /// <summary>
        /// 온실코드 추가
        /// </summary>
        /// <param name="data"></param>
        public void Insert(HouseData data)
        {
            Houses.Add(data);
            XmlHelper.Save(this);
        }
        
        #endregion

    }

    [XmlType("HouseData")]
    public class HouseData : BindableBase
    {
        /// <summary>
        /// 온실코드
        /// </summary>
        private string m_HouseCode;
        public string HouseCode
        {
            get
            {
                return m_HouseCode;
            }
            set
            {
                m_HouseCode = value;
                RaisePropertyChanged("HouseCode");
            }
        }

        /// <summary>
        /// 온실명
        /// </summary>
        private string m_HouseCodeName;
        public string HouseCodeName
        {
            get
            {
                return m_HouseCodeName;
            }
            set
            {
                m_HouseCodeName = value;
                RaisePropertyChanged("HouseCodeName");
            }
        }

        /// <summary>
        /// 온실동코드
        /// </summary>
        private string m_HouseNoCode;
        public string HouseNoCode
        {
            get
            {
                return m_HouseNoCode;
            }
            set
            {
                m_HouseNoCode = value;
                RaisePropertyChanged("HouseNoCode");
            }
        }

        /// <summary>
        /// 온실동코드명
        /// </summary>
        private string m_HouseNoCodeName;
        public string HouseNoCodeName
        {
            get
            {
                return m_HouseNoCodeName;
            }
            set
            {
                m_HouseNoCodeName = value;
                RaisePropertyChanged("HouseNoCodeName");
            }
        }

    }
    
}
