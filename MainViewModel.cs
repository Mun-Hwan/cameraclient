﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Input;

namespace CameraClient
{
    public class MainViewModel : BindableBase
    {
        #region property (bindable)

        /// <summary>
        /// 창 닫힘 여부
        /// </summary>
        private bool m_IsWindowClosing = false;
        public bool IsWindowClosing
        {
            get
            {
                return m_IsWindowClosing;
            }
            set
            {
                m_IsWindowClosing = value;
                RaisePropertyChanged("IsWindowClosing");
            }
        }

        /// <summary>
        /// 카메라 타입
        /// 0:R200, 1:D415
        /// </summary>
        private int m_SelectedCameraType = 0;
        public int SelectedCameraType
        {
            get
            {
                return m_SelectedCameraType;
            }
            set
            {
                m_SelectedCameraType = value;
                RaisePropertyChanged("SelectedCameraType");
            }
        }

        #endregion

        #region command


        /// <summary>
        /// 로딩 커맨드
        /// </summary>
        public DelegateCommand<MainWindow> LoadedCommand
        {
            get
            {
                return new DelegateCommand<MainWindow>(delegate (MainWindow window)
                {
                    SharedPreference.Instance.Config = XmlHelper.Load();

                    Task.Factory.StartNew(() =>
                    {
                        var now = DateTime.Now;
                        while (!IsWindowClosing)
                        {
                            now = DateTime.Now;

                            SharedPreference.Instance.CurrentTimeStamp = string.Format("{0:D4}-{1:D2}-{2:D2} {3:D2}:{4:D2}:{5:D2}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);

                            System.Threading.Thread.Sleep(1000);
                        }
                    });
                });
            }
        }

        /// <summary>
        /// 창 닫힘
        /// </summary>
        public DelegateCommand ClosingCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    IsWindowClosing = true;
                });
            }
        }

        /// <summary>
        /// 옵션 창 보이기
        /// </summary>
        public DelegateCommand ShowOptionCommand
        {
            get
            {
                return new DelegateCommand(delegate ()
                {
                    var window = new OptionWindow();
                    window.TB_HOST.Text = SharedPreference.Instance.Config.Host;
                    window.TB_PORT.Text = $"{SharedPreference.Instance.Config.Port}";

                    switch (SharedPreference.Instance.Config.SendType)
                    {
                        case 1: window.RB1.IsChecked = true; break;
                        case 2: window.RB2.IsChecked = true; break;
                        case 3: window.RB3.IsChecked = true; break;
                    } // end switch

                    if (window.ShowDialog() == true)
                    {
                        SharedPreference.Instance.Config.Host = window.TB_HOST.Text;
                        SharedPreference.Instance.Config.Port = Convert.ToInt32(window.TB_PORT.Text);
                        SharedPreference.Instance.Config.SendType = window.RB1.IsChecked.Value ? 1 : window.RB2.IsChecked.Value ? 2 : 3;

                        XmlHelper.Save(SharedPreference.Instance.Config);
                    }

                });
            }
        }

        /// <summary>
        /// 업로드 파일
        /// </summary>
        public DelegateCommand UploadFileCommand
        {
            get
            {
                return new DelegateCommand(async delegate ()
                {
                    if (SharedPreference.Instance.Config.SelectedHouse == null)
                    {
                        MessageBox.Show("온실코드를 선택해주세요.");
                        return;
                    }

                    string[] paths = null;

                    // 폴더 지정
                    if (SharedPreference.Instance.Config.SendType == 2)
                    {
                        var dlg = new System.Windows.Forms.FolderBrowserDialog();
                        dlg.ShowNewFolderButton = false;
                        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            paths = System.IO.Directory.GetFiles(dlg.SelectedPath);
                        }
                    }

                    // 파일 선택
                    else if (SharedPreference.Instance.Config.SendType == 3)
                    {
                        var dlg = new System.Windows.Forms.OpenFileDialog();
                        dlg.Multiselect = true;
                        dlg.RestoreDirectory = true;
                        dlg.ShowReadOnly = true;
                        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            paths = dlg.FileNames;
                        }
                    }

                    if (paths == null)
                    {
                        return;
                    }

                    Mouse.OverrideCursor = Cursors.Wait;

                    SharedPreference.Instance.ResSmartFarmsInit();

                    foreach (var item in paths)
                    {
                        var returnStr = await SharedPreference.Instance.HttpSendImageAsync(item);
                        var farm_info = JsonConvert.DeserializeObject<ResSmartFarm>(returnStr);

                        if (farm_info.DiagnosticInfo != null)
                        {
                            farm_info.PictureInfo.FileLocation = item;
                        }
                        else
                        {
                            farm_info.PictureInfo = new PictureInfo()
                            {
                                FileLocation = item
                            };
                        }

                        SharedPreference.Instance.ResSmartFarm = farm_info;
                    }

                    SharedPreference.Instance.ResSmartFarmsLength = paths.Length;

                    //Task.WaitAll();

                    Mouse.OverrideCursor = null;

                    var window = new ResultWindow();
                    var resultDlg = window.ShowDialog();

                });
            }
        }

        #endregion

        #region method

        #endregion

    }
}
