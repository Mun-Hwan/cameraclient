﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CameraClient
{
    public class XmlHelper
    {
        public const string LOCALREPOSITORY_FILENAME = "CameraClient.v4.xml";

        public static void Save(XmlData data)
        {
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            path += $@"\{LOCALREPOSITORY_FILENAME}";

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }

            using (StreamWriter wr = new StreamWriter(path))
            {
                XmlSerializer xs = new XmlSerializer(typeof(XmlData));
                xs.Serialize(wr, data);
            }
        }

        public static XmlData Load()
        {
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            path += $@"\{LOCALREPOSITORY_FILENAME}";

            //string path = $@"{Defines.LOCALREPOSITORY_PATH}\{Defines.LOCALREPOSITORY_FILENAME}";
            if (!File.Exists(path))
            {
                var data = new XmlData();
                data.CreateInitValues();
                Save(data);
            }

            using (var reader = new StreamReader(path))
            {
                XmlSerializer xs = new XmlSerializer(typeof(XmlData));
                return (XmlData)xs.Deserialize(reader);
            }
        }
    }
}
