﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CameraClient
{
    public class SharedPreference : BindableBase
    {
        #region single-ton

        private SharedPreference()
        {

        }

        private static SharedPreference m_Instance;
        public static SharedPreference Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new SharedPreference();
                }
                return m_Instance;
            }
            set
            {
                m_Instance = value;
            }
        }


        #endregion

        #region property

        private XmlData m_Config;
        public XmlData Config
        {
            get
            {
                return m_Config;
            }
            set
            {
                m_Config = value;
                RaisePropertyChanged("Config");

                if (Config.SelectedHouse != null)
                {
                    Config.SelectedHouse = Config.Houses.SingleOrDefault(p => p.HouseCode.Equals(Config.SelectedHouse.HouseCode) && p.HouseNoCode.Equals(Config.SelectedHouse.HouseNoCode));
                }
            }
        }
        
        private int m_resSmartFarmIdx = 0;
        public int ResSmartFarmIdx
        {
            get
            {
                return m_resSmartFarmIdx;
            }
            set
            {
                m_resSmartFarmIdx = value;
                RaisePropertyChanged("ResSmartFarmPage");
                RaisePropertyChanged("ResSmartFarm");
            }
        }

        private int m_resSmartFarmsLength = 0;
        public int ResSmartFarmsLength
        {
            get
            {
                return m_resSmartFarmsLength;
            }
            set
            {
                m_resSmartFarmsLength = value;
                RaisePropertyChanged("ResSmartFarmPage");
            }
        }

        public string ResSmartFarmPage
        {
            get
            {
                var str = new StringBuilder();
                str.Append(ResSmartFarmIdx + 1);
                str.Append(" / ");
                str.Append(m_resSmartFarmsLength);

                return str.ToString();
            }
        }

        private List<ResSmartFarm> m_resSmartFarms = new List<ResSmartFarm>();
        public List<ResSmartFarm> ResSmartFarms
        {
            get
            {
                return m_resSmartFarms;
            }
        }

        public void ResSmartFarmsInit()
        {
            m_resSmartFarms.Clear();
            m_resSmartFarmIdx = 0;
        }

        public ResSmartFarm ResSmartFarm
        {
            get
            {
                return ResSmartFarms[m_resSmartFarmIdx];
            }
            set
            {
                m_resSmartFarms.Add(value);
                RaisePropertyChanged("ResSmartFarm");
            }
        }


        /// <summary>
        /// 작물번호
        /// </summary>
        private ObservableCollection<string> m_RobotPositions;
        public ObservableCollection<string> RobotPositions
        {
            get
            {
                if (m_RobotPositions == null)
                {
                    m_RobotPositions = new ObservableCollection<string>();
                    for (int i = 1; i <= 25; ++i)
                    {
                        m_RobotPositions.Add($"{10000 + i}");
                    }
                }
                return m_RobotPositions;
            }
            set
            {
                m_RobotPositions = value;
                RaisePropertyChanged("RobotPositions");
            }
        }

        /// <summary>
        /// 촬영방향
        /// </summary>
        private ObservableCollection<KeyValuePair<string, string>> m_CameraDirection;
        public ObservableCollection<KeyValuePair<string, string>> CameraDirection
        {
            get
            {
                if (m_CameraDirection == null)
                {
                    m_CameraDirection = new ObservableCollection<KeyValuePair<string, string>>();

                    m_CameraDirection.Add(new KeyValuePair<string, string>("10", "Top1"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("11", "Top2"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("12", "Top3"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("13", "Top4"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("14", "Top5"));

                    m_CameraDirection.Add(new KeyValuePair<string, string>("20", "1_Side1"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("21", "1_Side2"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("22", "1_Side3"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("23", "1_Side4"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("24", "1_Side5"));

                    m_CameraDirection.Add(new KeyValuePair<string, string>("30", "2_Side1"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("31", "2_Side2"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("32", "2_Side3"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("33", "2_Side4"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("34", "2_Side5"));

                    m_CameraDirection.Add(new KeyValuePair<string, string>("40", "3_Side1"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("41", "3_Side2"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("42", "3_Side3"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("43", "3_Side4"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("44", "3_Side5"));

                    m_CameraDirection.Add(new KeyValuePair<string, string>("50", "4_Side1"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("51", "4_Side2"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("52", "4_Side3"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("53", "4_Side4"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("54", "4_Side5"));

                    m_CameraDirection.Add(new KeyValuePair<string, string>("60", "5_Side1"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("61", "5_Side2"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("62", "5_Side3"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("63", "5_Side4"));
                    m_CameraDirection.Add(new KeyValuePair<string, string>("64", "5_Side5"));

                    m_CameraDirection.Add(new KeyValuePair<string, string>("90", "파노라마"));
                }
                return m_CameraDirection;
            }
            set
            {
                m_CameraDirection = value;
                RaisePropertyChanged("CameraDirection");
            }
        }

        /// <summary>
        /// 현재 시각
        /// </summary>
        private string m_CurrentTimeStamp;
        public string CurrentTimeStamp
        {
            get
            {
                return m_CurrentTimeStamp;
            }
            set
            {
                m_CurrentTimeStamp = value;
                RaisePropertyChanged("CurrentTimeStamp");
            }
        }

        #endregion

        #region method

        /// <summary>
        /// 이미지 전송
        /// </summary>
        /// <param name="path"></param>
        async public void SendImage(string path)
        {
            string filename = System.IO.Path.GetFileName(path);

            await Task.Factory.StartNew(() =>
            {
                ImageClientLib.ImageClient client = new ImageClientLib.ImageClient();
                var result = client.Connect(Config.Host, Config.Port);

                if (result)
                {
                    client.SendImage(ImageClientLib.SendFileTypes.IMAGE_RGB, filename, System.IO.File.ReadAllBytes(path));
                    client.ReceiveResult();
                }
                else
                {

                }

                client.Close();
            });
        }

        /// <summary>
        /// 이미지 전송
        /// </summary>
        /// <param name="paths"></param>
        async public void SendImages(string[] paths)
        {
            await Task.Factory.StartNew(() =>
            {
                foreach (var path in paths)
                {
                    string filename = System.IO.Path.GetFileName(path);

                    ImageClientLib.ImageClient client = new ImageClientLib.ImageClient();
                    var result = client.Connect(Config.Host, Config.Port);

                    if (result)
                    {
                        client.SendImage(ImageClientLib.SendFileTypes.IMAGE_RGB, filename, System.IO.File.ReadAllBytes(path));
                        client.ReceiveResult();
                    }

                    client.Close();

                    System.Threading.Thread.Sleep(10);

                } // end foreach
            });
        }

        /// <summary>
        /// 이미지 전송
        /// </summary>
        /// <param name="path"></param>
        async public Task<string> HttpSendImageAsync(string path)
        {
            //var address = $@"http://192.168.219.103:8080/leaf/test1";
            var host = Config.Host;
            var port = Config.Port;
            var addrStr = new StringBuilder();
            addrStr.Append($@"http://");
            addrStr.Append(host);
            addrStr.Append($@":");
            addrStr.Append(port);
            addrStr.Append($@"/Analysis/Leaf");

            var resultString = string.Empty;

            string x = await Task.Factory.StartNew(() =>
            {
                using (var stream = File.Open(path, FileMode.Open))
                {
                    var files = new[]
                    {
                        new HttpUploadFile
                        {
                            Name = "files",
                            Filename = Path.GetFileName(path),
                            ContentType = "text/plain",
                            Stream = stream
                        }
                    };

                    var values = new NameValueCollection
                    {
                        { "client", "VIP" },
                        { "name", "John Doe" },
                    };

                    byte[] result = new HttpUploadFile().HttpUploadFiles(addrStr.ToString(), files, values);
                    resultString = Encoding.UTF8.GetString(result);

                    return resultString;
                }
            });

            return x;
        }

        public string HttpSendImage(string path)
        {
            //var address = $@"http://192.168.219.103:8080/leaf/test1";
            var host = Config.Host;
            var port = Config.Port;
            var addrStr = new StringBuilder();
            addrStr.Append($@"http://");
            addrStr.Append(host);
            addrStr.Append($@":");
            addrStr.Append(port);
            addrStr.Append($@"/Analysis/Leaf");

            var resultString = string.Empty;

            using (var stream = File.Open(path, FileMode.Open))
            {
                var files = new[]
                {
                        new HttpUploadFile
                        {
                            Name = "files",
                            Filename = Path.GetFileName(path),
                            ContentType = "text/plain",
                            Stream = stream
                        }
                    };

                var values = new NameValueCollection
                    {
                        { "client", "VIP" },
                        { "name", "John Doe" },
                    };

                byte[] result = new HttpUploadFile().HttpUploadFiles(addrStr.ToString(), files, values);
                resultString = Encoding.UTF8.GetString(result);

                return resultString;
            }
        }

        #endregion
    }
}
